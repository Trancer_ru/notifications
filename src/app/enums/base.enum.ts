import {BehaviorSubject} from 'rxjs';

export interface IEnumItem {
	id?: number;
	text?: string;
};
export interface IEnum {
	[enumKey: number]: string;
};

export interface IEnumGeneral {
	[enumKey: number]: boolean;
};

export interface IEnumValues {
	[enumName: string]: IEnumItem[];
};

export interface IDictionary {
	[key: string]: string | number | boolean;
}

export const ENUM_VALUES$ = new BehaviorSubject<IEnumValues>({});


export function getEnumObjectTexts<T = IDictionary>(
	enumName: string,
	defaultEnum?: IEnum,
	enumFilters?: Partial<T>
) {
	const ENUM_VALUES = ENUM_VALUES$.getValue();
	if (defaultEnum) {
		return defaultEnum;
	}
	if (ENUM_VALUES[enumName]) {
		const texts: IEnum = {};
		ENUM_VALUES[enumName].filter(item => {
			if (!enumFilters) {
				return true;
			}
			return Object.keys(item).filter(key =>
				enumFilters[key] !== undefined && enumFilters[key] === item[key]
			).length > 0
		}).forEach(
			item => {
				texts[item.id] = item.text || (item as any).name;
				texts[item.id] = (texts[item.id] && texts[item.id][0] === '_') ? String(texts[item.id]).substr(1) : texts[item.id]
			});
		return texts;
	}
	return {};
}
export function getEnumObjects<T = IDictionary>(
	enumName: string,
	defaultEnum?: IEnum,
	enumFilters?: Partial<T>
) {
	const ENUM_VALUES = ENUM_VALUES$.getValue();
	if (defaultEnum) {
		const items: IEnumItem[] = [];
		Object.keys(defaultEnum).forEach(
			key =>
				items.push({
					id: +key,
					text: (items[key] && items[key][0] === '_') ? String(items[key]).substr(1) : items[key]
				})
		)
		return items;
	}
	if (ENUM_VALUES[enumName]) {
		return ENUM_VALUES[enumName].filter(item => {
			if (!enumFilters) {
				return true;
			}
			return Object.keys(item).filter(key =>
				enumFilters[key] !== undefined && enumFilters[key] === item[key]
			).length > 0
		});
	}
	return;
}

export interface IBaseEnum {
	key: number|string;
	value: string;
	selected: boolean;
	general: boolean;
	original: any;
}
export class BaseEnum implements IBaseEnum {
	key: number|string;
	value: string;
	selected: boolean;
	general: boolean;
	original: any;
	constructor(
		public enumName: string,
		public enumObj?: IEnum,
		public enumTexts?: IEnum,
		public enumGeneral?: IEnumGeneral
	) {}

	parse(
		data: any
	) {
		const enumObj = getEnumObjects(this.enumName, this.enumObj);
		const enumTexts = getEnumObjectTexts(this.enumName, this.enumTexts);
		if (data.id !== undefined) {
			this.key = +data.id;
		}
		if (data.key !== undefined) {
			this.key = +data.key;
		}
		if (data.id === undefined && data.key === undefined) {
			this.key = +data;
		}
		this.value = enumTexts[this.key] || enumTexts[data];
		this.general = this.enumGeneral ? !!(this.enumGeneral[this.key] || this.enumGeneral[data]) : true;
		this.original =
			enumObj && enumObj.filter(item => item.id === this.key)[0];
		if (this.key && !(enumObj && enumObj.filter(item => item.id === this.key).length > 0)) {
			this.value = 'неверное значение словаря';
			console.warn(`Invalid key for ${this.enumName} (${JSON.stringify({
				key: this.key, available: enumObj ? enumObj.map(item => `${item.id}:${item.text}`) : enumObj
			})!}`)
		}
	}
	toString() {
		return this.value;
	}
}
