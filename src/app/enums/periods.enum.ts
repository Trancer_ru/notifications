import {BaseEnum, getEnumObjectTexts} from './base.enum';

export enum PeriodEnum {
	Minute = 1,
	Hour = 2,
	Day = 3,
	Month = 4
}

export const periodDeclension = {
	[PeriodEnum.Minute]: ['Минута', 'Минуты', 'Минут'],
	[PeriodEnum.Hour]: ['Час', 'Часа', 'Часов'],
	[PeriodEnum.Day]: ['День', 'Дня', 'Дней'],
	[PeriodEnum.Month]: ['Месяц', 'Месяца', 'Месяцев']
}

export const periodMomentKeys = {
	[PeriodEnum.Minute]: 'm',
	[PeriodEnum.Hour]: 'h',
	[PeriodEnum.Day]: 'd',
	[PeriodEnum.Month]: 'M'
}
export const periodEveryKeys = {
	[PeriodEnum.Minute]: 'minute',
	[PeriodEnum.Hour]: 'hour',
	[PeriodEnum.Day]: 'day',
	[PeriodEnum.Month]: 'month'
}

export class Period extends BaseEnum {
	declension: string[];
	declensionSmall: string[];
	momentKeys: string;
	everyKeys: string;
	constructor(data?: any) {
		super(
			'period',
			PeriodEnum
		);
		if (data === undefined) {
			return;
		}
		if (typeof data !== 'number' && (typeof data === 'object')) {
			Object.assign(this, data);
		} else {
			if (typeof data === 'number') {
				data = { id: data };
			}
			this.parse(data);
		}
		this.momentKeys = periodMomentKeys[this.key];
		this.everyKeys = periodEveryKeys[this.key];
		this.declension = periodDeclension[this.key];
		this.declensionSmall = periodDeclension[this.key] && periodDeclension[this.key].map(item => item.toLowerCase());
	}
}


export function getPeriods() {
	return Object.keys(
		getEnumObjectTexts('period', PeriodEnum)
	).filter(key => !isNaN(+key)).map(key => new Period(key));
};

export function periodByKey(key: number) {
	return getPeriods().filter(en => en.key === key)[0];
}
