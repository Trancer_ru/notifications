import { Injectable } from '@angular/core';
import {NotificationsStore} from '@stores/notifications.store';
import {NotificationModel} from '@root/app/models/notification';
import {ILocalNotification, LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {LSService} from '@services/ls.service';
import {interval} from 'rxjs';
import {take} from 'rxjs/operators';

@Injectable()
export class StoresService {
	constructor(
		private _notificationsStore: NotificationsStore,
		private _localNotifications: LocalNotifications,
		private _ls: LSService
	){}

	loadAll() {
		this._getAll();
		interval(1000).pipe(take(3)).subscribe(_ => this._getAll());

		/*
		this._localNotifications.clearAll().then( _ => this._localNotifications.cancelAll().then(_ => {
			const notes = this._ls.getData('notifications');
			if(notes && Array.isArray(notes)) {
				this._notificationsStore.set(notes.map(note => new NotificationModel(note)));
			}

		}));
		 */
	}

	private _getAll(){
		const notes = [];
		const ids = [];

		this._localNotifications.getAll().then( (n: ILocalNotification[]) => {
			n.forEach(note => {
				let n = new NotificationModel(note.data);
				if(!ids.includes(n.id)) {
					ids.push(n.id);
					notes.push(n)
				}
			});
			this._notificationsStore.set(notes);
			this._notificationsStore.saveInStorage();
		});
	}
}
