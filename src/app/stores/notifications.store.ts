import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import {NotificationModel} from '@root/app/models/notification';
import {LSService} from '@services/ls.service';
import {NotificationsCheckService} from '@services/notifications-check.service';

@Injectable()
export class NotificationsStore {
    notifications$ = new BehaviorSubject<NotificationModel[]>([]);

    constructor(
        private _ls: LSService,
        private _notificationCheck: NotificationsCheckService
    ){}

    set(notes: NotificationModel[]) {
        this.notifications$.next(notes);
    }

    add(note: NotificationModel) {
        const notes = this.notifications$.value;
        notes.push(note);
        this.notifications$.next(notes);
        this.saveInStorage();
        this._notificationCheck.add(note);
    }

    remove(id: number) {
        const notes = this.notifications$.value.filter(note => note.id != id);
        this.notifications$.next(notes);
        this.saveInStorage();
        this._notificationCheck.remove(id);
    }

    edit(editedNote: NotificationModel) {
        const notes = this.notifications$.value.map(note => note.id === editedNote.id ? editedNote : note);
        this.notifications$.next(notes);
        this.saveInStorage();
        this._notificationCheck.edit(editedNote);
    }

    saveInStorage(){
        this._ls.setData('notifications', this.notifications$.value.map(note => note.toJSON()));
    }
}
