import {NotificationsStore} from './notifications.store';
import {StoresService} from './_stores.service';

export const STORE_PROVIDERS = [
	StoresService,
	NotificationsStore
];
