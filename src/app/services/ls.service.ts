import { Injectable } from "@angular/core";
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class LSService {
	constructor(private _ls: LocalStorageService) {};

	// Установка значения в LS
	setData(key: string, val: any): void {
		this._ls.set(key, val);
	};

	// Получение значения из LS
	getData(key: string): any {
		const data = this._ls.get(key);
		return data === 'null' ? null : data;
	};

	// Удаление значения из LS
	removeData(key: string): void {
		this._ls.remove(key);
	};
};
