import {LSService} from "./ls.service";
import {ModalService} from "./modal.service";
import {AlertService} from './alert.service';
import {NotificationsCheckService} from './notifications-check.service';
import {ConfigService} from './config.service';

export const SERVICE_PROVIDERS = [
    LSService,
	ModalService,
	AlertService,
	NotificationsCheckService,
	ConfigService
];
