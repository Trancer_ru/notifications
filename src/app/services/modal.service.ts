import { Injectable } from "@angular/core";
import { ModalController} from '@ionic/angular';

interface IModalProperties {
	inputData?: any,
	cssClass?: string,
	swipeable?: boolean,
	zoomInAnimation?: boolean
}

@Injectable()
export class ModalService {
	constructor(
		private _modalController: ModalController
	) {};

	async show(component, props?: IModalProperties) {
		const modal = await this._modalController.create({
			component: component,
			componentProps: props.inputData,
			cssClass: (props.cssClass || undefined),
			showBackdrop: false,
			swipeToClose: true,
			presentingElement: props.swipeable ? document.getElementById('#root-router') : undefined
		});
		await modal.present();
		return modal;
	}
};
