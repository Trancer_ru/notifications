import { Injectable } from "@angular/core";
import {NotificationModel} from '@root/app/models/notification';
import {ILocalNotification, ILocalNotificationTrigger, LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {ConfigService} from '@services/config.service';
import {Platform} from '@ionic/angular';

@Injectable()
export class NotificationsCheckService {
	constructor(
		private _localNotifications: LocalNotifications,
		private _config: ConfigService,
		private _platform: Platform
	) {};

	list: ILocalNotification[] = [];

	init(){
		this._localNotifications.on('add').subscribe(data => {
			console.log("-------ADD EVENT", data);
		});
		this._localNotifications.on('update').subscribe(data => {
			console.log("-------UPDATE EVENT", data);
		});
		this._localNotifications.on('clear').subscribe(data => {
			console.log("-------CLEAR EVENT", data);
		});

		this._localNotifications.on('trigger').subscribe(ln => {
			console.log("=======TRIGGER EVENT");
			this.add(new NotificationModel(ln.data));
		});
		this._localNotifications.on('click').subscribe(ln => {
			console.log("=======CLICK EVENT");
			this.add(new NotificationModel(ln.data));
		});
		this._platform.resume.subscribe(() => {
			console.log("=======RESUME EVENT");
			//проверим все напоминания
			this._checkAll();
		});
	}

	add(note: NotificationModel){
		this._localNotifications.schedule(this._makeEvent(note));
	}

	edit(note: NotificationModel){
		this._localNotifications.get(note.id).then( n => {
			this._localNotifications.update(this._makeEvent(note, n));
		});
	}

	remove(noteId: number){
		this._localNotifications.clear(noteId).then(_ => this._localNotifications.cancel(noteId))
	}

	private _checkAll(){
		this._localNotifications.getAll().then( notes => {
			notes.forEach(note => this._localNotifications.update(this._makeEvent(new NotificationModel(note.data), note)))
		})
	}

	private _makeEvent(note: NotificationModel, current: ILocalNotification = {}): ILocalNotification {

		let trigger: ILocalNotificationTrigger = {at: new Date(note.startFullDate)};

		// Если есть периодичность
		if(note.periodTypeId) {
			if(note.past) trigger = <ILocalNotificationTrigger>{unit: note.periodType.everyKeys, every: note.periodNum};
			/*
			if(!this._config.deviceInfo.isIos) {
				trigger = <ILocalNotificationTrigger> {firstAt: new Date(note.startFullDate), unit: note.periodType.everyKeys, every: note.periodNum};
			} else if(note.past) trigger = <ILocalNotificationTrigger>{unit: note.periodType.everyKeys, every: note.periodNum};
			
			 */
		}
		//{firstAt: new Date(note.startFullDate), unit: note.periodType.everyKeys, every: note.periodNum},

		return <ILocalNotification> {
			...current,
			id: note.id,
			title: 'Напоминание',
			text: note.name,
			trigger: trigger,
			data: note.toJSON(),
			foreground: false,
			priority: 2,
			autoClear: true,
			silent: false,
			sticky: true
		};
	}
};
