import { Injectable } from "@angular/core";
import { AlertController } from '@ionic/angular';

export interface IAlertButton {
	text: string,
	role?: 'cancel' | '',
	cssClass?: string,
	handler?: Function | any
}

export interface IAlert {
	header?: string;
	subHeader?: string;
	message?: string;
	backdropDismiss?: boolean;
	buttons?: (IAlertButton | any)[];
}

@Injectable()
export class AlertService {
	constructor(
		private _alertController: AlertController,
	) {};

	async show(data: IAlert){

		const alert = await this._alertController.create({
			backdropDismiss: typeof data.backdropDismiss !== 'undefined' ? data.backdropDismiss : true,
			header: data.header || '',
			subHeader: data.subHeader || undefined,
			message: data.message || '',
			buttons: data.buttons || ['Закрыть']
		});

		await alert.present();
	}
};
