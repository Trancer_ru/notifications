import { Injectable } from "@angular/core";
import {Platform} from '@ionic/angular';

@Injectable()
export class ConfigService {
	deviceInfo: {
		platform?: 'ios' | 'android',
		isIos?: boolean,
	} = {};


	constructor(
		private _platform: Platform
	) {};

	init(){
		this.deviceInfo.isIos = this._platform.is('ios');
	}
};
