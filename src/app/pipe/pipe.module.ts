import { NgModule } from "@angular/core";
import { InvalidText } from "./validator-messages.pipe";

const PIPES = [
	InvalidText,
];

@NgModule({
	imports: [],
	declarations: [
		...PIPES
	],
	exports: [
		...PIPES
	],
})

export class PipeModule { }
