import { Pipe, PipeTransform } from "@angular/core";
import {validatorMessages} from '@root/app/utils/validator-messages';

@Pipe({
	name: "invalidText"
})
export class InvalidText implements PipeTransform {
	public transform(validator: string, fieldTag: string) {
		return validatorMessages(fieldTag, validator);
	}
}
