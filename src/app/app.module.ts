import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {LocalStorageModule} from 'angular-2-local-storage';

import { SERVICE_PROVIDERS } from "@services/index";
import {STORE_PROVIDERS} from '@stores/index';


@NgModule({
	declarations: [AppComponent],
	entryComponents: [],
	imports: [
		BrowserModule,
		IonicModule.forRoot({
			backButtonText: '',
			swipeBackEnabled: false,
			hardwareBackButton: true
		}),
		AppRoutingModule,
		LocalStorageModule.forRoot({
			prefix: "NOTESAPP",
			storageType: "localStorage"
		}),
	],
	providers: [
		StatusBar,
		SplashScreen,
		LocalNotifications,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
		...SERVICE_PROVIDERS,
		...STORE_PROVIDERS
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
