import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {StoresService} from '@stores/_stores.service';
import {NotificationsCheckService} from '@services/notifications-check.service';
import {ConfigService} from '@services/config.service';

@Component({
	selector: 'app-root',
	template: '<ion-app><ion-router-outlet></ion-router-outlet></ion-app>'
})
export class AppComponent {
	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		private _storesService: StoresService,
		private _notificationService: NotificationsCheckService,
		private _config: ConfigService
	) {
		this.initializeApp();
	}

	initializeApp() {
		this.platform.ready().then(() => {
			this._config.init();
			this.statusBar.styleDefault();
			this.splashScreen.hide();
			this._storesService.loadAll();
			this._notificationService.init();
		});
	}
}
