import {Period} from '@root/app/enums/periods.enum';
import * as moment from 'moment';

export class NotificationModel {
    id: number = undefined;
    name: string = undefined;
    startDate: string = undefined;
    startTime: string = undefined;
    periodNum: number = undefined;
    periodTypeId: number = undefined;

    startFullDate: string;
    periodType: Period;
    past: boolean = undefined;

    constructor(data?: Partial<NotificationModel>) {
        if(typeof data === 'string') data = JSON.parse(data);
        if (!data) {
            return;
        }
        if ((data instanceof NotificationModel)) {
            Object.assign(this, data);
        } else {
            const newObject = new NotificationModel();
            Object.keys(newObject).forEach(key =>
                this[key] = (data[key] === undefined && !!newObject[key] === newObject[key]) ? newObject[key] : data[key]
            );
            this.startFullDate = moment(moment(this.startDate).format("YYYY-MM-DD") + ' ' + moment(this.startTime).format("HH:mm")).format();
            this.periodType = new Period(data.periodTypeId);
            this.id = data.id || Math.round(10000000*Math.random());
            this.past = moment(this.startFullDate) < moment();
        }
    }

    toJSON() {
        const data: any = {};
        Object.keys(new NotificationModel()).forEach(key =>
            data[key] = this[key]
        );
        return data;
    }
}
