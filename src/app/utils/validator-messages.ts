export function validatorMessages(fieldTag: string, key: string): string {
	return (messages[fieldTag] && messages[fieldTag][key]) || key;
}

const messages = {
	"name": {
		required: "Введите название уведомления"
	},
	"startDate": {
		required: "Введите дату"
	},
	"startTime": {
		required: "Введите время"
	},
	"period": {
		required: "Выберите период срабатывания"
	},
}
