export function transformDateToRu(date: Date) {
    if (date && date.getDate && date.getMonth && date.getFullYear && !isNaN(date.getFullYear())) {
        const curr_date = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        const curr_month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
        const curr_year = date.getFullYear();
        return curr_date + '.' + curr_month + '.' + curr_year;
    }
    return '';
} export function transformDateToUtc(date: Date) {
    const value = transformDateToRu(date);
    return value ? transformRuToUTC(value) : value;
}
export function treatAsUTC(date: string | Date): any {
    var result = typeof date === 'string' ? new Date(date) : date;
    result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
    return result;
}

export function daysBetween(startDate: string | Date, endDate: string | Date = new Date(), needRound: boolean = true) {
    const millisecondsPerDay = 24 * 60 * 60 * 1000;
    const days = (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
    return needRound ? Math.round(days) : days;
}
export function transformUTCToDate(str: string | Date) {
    return (str && !(str instanceof Date) && str.substring) ? new Date(str.substring(0, 10)) : str;
}
export function transformUTCToRu(str: string) {
    if (str && str.length === 10 && str.split('.').length === 3) {
        return str;
    }
    const value: any = str && str.substring ? new Date(str.substring(0, 10)) : str;

    if (value && value.getDate && value.getMonth && value.getFullYear && !isNaN(value.getFullYear())) {
        const curr_date = value.getDate() < 10 ? `0${value.getDate()}` : value.getDate();
        const curr_month = value.getMonth() + 1 < 10 ? `0${value.getMonth() + 1}` : value.getMonth() + 1;
        const curr_year = value.getFullYear();
        return curr_date + '.' + curr_month + '.' + curr_year;
    }
    return str;
}
export function transformRuToUTC(str: string) {
    if (str && str.length === 8) {
        str = str[0] + str[1] + '.' + str[2] + str[3] + '.' + str[4] + str[5] + str[6] + str[7];
    }
    if (str && str.length === 10 && str.split('.').length === 3) {
        const value = str.split('.');
        const curr_date = value[0];
        const curr_month = value[1];
        const curr_year = value[2];
        return curr_year + '-' + curr_month + '-' + curr_date;
    }
    return str;
}

export function getStringMonthAndYear(date: string) {
    const curr_date = new Date(date);
    const month = curr_date.toLocaleString('default', { month: 'long' });
    return month + ' ' + curr_date.getFullYear();
}
