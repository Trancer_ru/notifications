import { getNoun } from "./number-utils";
import * as moment from "moment";

export const monthsDictionary = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
export const months2Dictionary = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
export const weekDayDictionary = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];

export function dayWithMonth(text: string): string {
    const textArray = text ? text.split('.') : [];
    if (textArray.length === 3) {
        const day = +textArray[0];
        const month = +textArray[1];
        const monthName = months2Dictionary[month - 1];
        return `${day} ${monthName}`;
    }
    return '';
}
export function agePeriod(from: number, to?: number): string {
    if (from && !to) {
        return getNoun(
            from,
            ' год',
            ' года',
            ' лет'
        );
    }
    if (from && to) {
        return `${from}-${to} ${getNoun(
            to,
            ' год',
            ' года',
            ' лет'
        )}`;
    }
    return '';
}


export function mathCurentDate(val: string): Date {
    let curentDate = new Date()
    if (val == 'week') {
        curentDate.setDate(curentDate.getDate() - 7);
    }
    else if (val == 'month') {
        curentDate.setMonth(curentDate.getMonth() - 1);
    }
    else if (val == 'threemonth') {
        curentDate.setMonth(curentDate.getMonth() - 3);
    }
    else if (val == 'reset') {
        curentDate = null;
    }
    return curentDate;
}
export function mathReverseCurentDate(date: Date): string {
    let week = new Date();
    let month = new Date();
    let thremonth = new Date();
    week.setDate(week.getDate() - 7);
    month.setMonth(month.getMonth() - 1);
    thremonth.setMonth(thremonth.getMonth() - 3);
    if (date.toDateString() == week.toDateString()) {
        return 'week'
    }
    else if (date.toDateString() == month.toDateString()) {
        return 'month'
    }
    else if (date.toDateString() == thremonth.toDateString()) {
        return 'thremonth'
    }
    else 'reset'
}

export function getStringFromDate(date?: Date, format?: string) {
    return moment(date).format(format);
}
