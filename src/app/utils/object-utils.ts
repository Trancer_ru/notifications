export function compareObjects(obj2: { [x: string]: any; }, obj1: { [x: string]: any; }) {
    if (obj2.latitude) {
        obj2.latitude = obj2.latitude.toFixed(3);
    }
    if (obj2.longitude) {
        obj2.longitude = obj2.longitude.toFixed(3);
    }
    if (obj1.latitude) {
        obj1.latitude = obj1.latitude.toFixed(3);
    }
    if (obj1.longitude) {
        obj1.longitude = obj1.longitude.toFixed(3);
    }
    const keys1 = Object.keys(obj1).filter(
        key => obj1[key] !== undefined
    );
    const keys2 = Object.keys(obj2).filter(
        key => obj2[key] !== undefined
    );
    const result = keys1.filter(key1 =>
        keys2.indexOf(key1) !== -1 &&
        obj1[key1] === obj2[key1]
    ).length === keys2.length;
    return result;
}