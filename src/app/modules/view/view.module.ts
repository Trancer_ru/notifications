import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { ViewComponent } from './view.component';
import {ViewRouter} from './view.router';

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		ViewRouter
	],
	declarations: [ViewComponent]
})
export class ViewModule {}
