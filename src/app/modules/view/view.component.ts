import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
	selector: 'view-notification',
	templateUrl: './view.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewComponent {

	constructor(
		private activatedRoute: ActivatedRoute
	) { }

	ngOnInit() {
		//const id = this.activatedRoute.snapshot.paramMap.get('id');
		//this.message = this.data.getMessageById(parseInt(id, 10));
	}
}
