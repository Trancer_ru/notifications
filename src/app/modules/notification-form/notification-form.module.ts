import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NotificationFormComponent } from "./notification-form.component";
import {IonicModule} from "@ionic/angular";
import {ReactiveFormsModule} from "@angular/forms";
import {ExtFormsModule} from '@modules/ext-forms/ext-forms.module';

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		ReactiveFormsModule,
		ExtFormsModule
	],
	declarations: [NotificationFormComponent],
	exports: [NotificationFormComponent]
})
export class NotificationFormModule {

}
