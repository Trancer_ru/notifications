import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {ModalController, PickerController} from '@ionic/angular';
import {NotificationModel} from '@root/app/models/notification';
import {getStringFromDate} from '@root/app/utils/date-utils';
import {getNoun} from '@root/app/utils/number-utils';
import {getPeriods, periodByKey} from '@root/app/enums/periods.enum';
import * as moment from 'moment';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  	selector: "notification-form",
  	templateUrl: "notification-form.html",
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [FormBuilder]
})
export class NotificationFormComponent {
	@Input() type: 'edit'|'add' = 'add';
	@Input() note: NotificationModel = new NotificationModel();

	minDate = getStringFromDate(new Date());
	minTime = getStringFromDate(new Date(moment().add(1, 'm').format()));
	maxDate = getStringFromDate(new Date(2100, 1, 1));
	form: any = FormGroup;
	private _destroyed$: Subject<boolean> = new Subject<boolean>();
	private _periods = getPeriods();
	private _num: {text: string, value: number}[] = [
		{text: '1', value: 1},
		{text: '2', value: 2},
		{text: '3', value: 3},
		{text: '4', value: 4},
		{text: '5', value: 5},
		{text: '6', value: 6},
		{text: '7', value: 7},
		{text: '8', value: 8},
		{text: '9', value: 9},
		{text: '10', value: 10},
		{text: '11', value: 11},
		{text: '12', value: 12},
		{text: '13', value: 13},
		{text: '14', value: 14},
		{text: '15', value: 15},
		{text: '16', value: 16},
		{text: '17', value: 17},
		{text: '18', value: 18},
		{text: '19', value: 19},
		{text: '10', value: 20},
		{text: '21', value: 21},
		{text: '22', value: 22},
		{text: '23', value: 23},
		{text: '24', value: 24},
		{text: '25', value: 25},
		{text: '26', value: 26},
		{text: '27', value: 27},
		{text: '28', value: 28},
		{text: '29', value: 29},
		{text: '30', value: 30}
	];

	constructor (
		private _fb: FormBuilder,
		private _modalCtrl: ModalController,
		private _pickerController: PickerController,
		private _cd: ChangeDetectorRef
	) {}

	ngOnInit(){
		this.initForm();
	}

	private initForm(): void {
		this.form = this._fb.group({
			id: this.note.id,
			name: [(this.note && this.note.name) || '', [Validators.required]],
			startDate: [this.note.startDate, [Validators.required]],
			startTime: [this.note.startTime, [Validators.required]],
			periodNum: [this.note.periodNum],
			periodTypeId: [this.note.periodTypeId],
			periodString: [this._getPeriodString(this.note.periodNum, this.note.periodTypeId)],
		});

		this.form.get('startDate').valueChanges
			.pipe(
				takeUntil(this._destroyed$)
			)
			.subscribe(value => {
				this.minTime = getStringFromDate(new Date(moment(value).startOf('day').add(1, 'm').format()));
			});
	}

	async setPeriod(){
		const picker = await this._pickerController.create({
			columns: this._getColumns(),
			buttons: [
				{
					text: 'Отмена',
					role: 'cancel'
				},
				{
					text: 'Выбрать',
					handler: value => this._choosePeriod(value)
				}
			]
		});
		picker.columns[0].selectedIndex = this.form.get('periodNum').value-1;
		picker.columns[1].selectedIndex = this.form.get('periodTypeId').value-1;

		await picker.present();
	}

	private _getColumns(){
		return [
			{name: 'start', options: this._getColumnOptions(1)},
			{name: 'end', options: this._getColumnOptions(2)},
		]
	}

	private _getColumnOptions(num){
		let options = [];
		if(num == 1) {
			this._num.forEach( item => {
				options.push({...item})
			});
		}
		if(num == 2) {
			this._periods.forEach( item => {
				options.push({
					text: item.declension[0],
					value: item.key
				})
			});
		}
		return options;
	}

	private _choosePeriod(values){
		let start = values.start.value;
		let end = values.end.value;
		this.form.patchValue({
			periodNum: start,
			periodTypeId: end,
			periodString: this._getPeriodString(start, end)
		});
		this._cd.detectChanges();
	}

	private _getPeriodString(periodNum: number, periodTypeId: number): string{
		if(periodNum && periodTypeId) return periodNum + ' ' + getNoun(periodNum, ...periodByKey(periodTypeId).declensionSmall);
		return '';
	}

	save(): void {
		const note = new NotificationModel(this.form.getRawValue());
		this.close(note);
	}

	clearPeriod(){
		this.form.patchValue({
			periodNum: undefined,
			periodTypeId: undefined,
			periodString: undefined
		});
	}

	// Закрытие окна
	close(data?: NotificationModel) {
		this._modalCtrl.dismiss(data);
	}

	ngOnDestroy() {
		this._destroyed$.next(true);
		this._destroyed$.complete();
	}
}
