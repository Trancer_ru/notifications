import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {AbstractControl} from "@angular/forms";
import {IExtSelectItem} from "../components/ext-select/ext-select.component";
import {BehaviorSubject} from 'rxjs';

@Component({
	selector: "form-item",
	templateUrl: "form-item.html",
	styleUrls: ['./form-item.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormItemComponent {
	@Input() control: AbstractControl;
	@Input() controlType: 'select'|'checkbox' = 'select';
	@Input() clear: boolean = true; // Показывать или нет иконку удаления выбранных значений

	//Для селекта
	@Input() dictionary: IExtSelectItem[] = [];
	@Input() keyAsValue: boolean = false;
	@Input() filterOption: boolean = false; //Строка ввода текста для фильтра
	@Input() emptyText: string = ''; //Строка при необязательности выбора из селекта

	noValues$ = new BehaviorSubject<boolean>(true);

	constructor() {}

	ngOnInit() {
		this.calcNoValues();
	}

	calcNoValues(){
		if(!this.control) this.noValues$.next(false);
		else {
			if(!this.clear) this.noValues$.next(true);
			else if(this.control.value == null) this.noValues$.next(true);
			else if(Array.isArray(this.control.value) && this.control.value.filter(item => item.selected).length == 0) this.noValues$.next(true);
			else this.noValues$.next(false);
		}
	}
}
