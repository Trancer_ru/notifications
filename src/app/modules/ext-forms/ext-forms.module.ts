import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExtSelectComponent } from './components/ext-select/ext-select.component';
import {IonicModule} from "@ionic/angular";
import {FormErrorsComponent} from '@modules/ext-forms/form-errors/form-errors.component';
import {FormItemComponent} from '@modules/ext-forms/form-item/form-item.component';
import {PipeModule} from '@root/app/pipe/pipe.module';


@NgModule({
	imports: [
  	    CommonModule,
		IonicModule,
        FormsModule,
        ReactiveFormsModule,
		PipeModule
	],
	declarations: [
		ExtSelectComponent,
		FormErrorsComponent,
		FormItemComponent
	],
	exports: [
		ExtSelectComponent,
		FormErrorsComponent,
		FormItemComponent
    ]
})
export class ExtFormsModule { }
