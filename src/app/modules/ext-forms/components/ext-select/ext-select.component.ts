import { ChangeDetectionStrategy, Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export interface IExtSelectItem {
	key: string | number | boolean;
	value: string;
}

@Component({
	selector: 'ext-select',
	templateUrl: './ext-select.html',
	styleUrls: ['./ext-select.scss'],
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => ExtSelectComponent),
		multi: true
	}],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExtSelectComponent implements ControlValueAccessor {
	@Input() filterOption: boolean = false;
	@Input() keyAsValue: boolean;
	@Input() dictionary: IExtSelectItem[] = [];
	@Input() emptyText: string = ''; //Строка при необязательности выбора из селекта

	currentItem: IExtSelectItem = null;
	currentValue: string | number | boolean = null;
	disable: boolean;

	filteredDictionary: IExtSelectItem[] = [];
	searchString: string = "";

	constructor(){}

	ngOnInit(){}

	applyFilter(event?){
		if(event) this._onChange(undefined);

		this.filteredDictionary = this.dictionary.filter( (item: IExtSelectItem) => {
			return !this.searchString || item.value.toLowerCase().includes(this.searchString.toLowerCase())
		});
	}

	currentItemChange(item: IExtSelectItem) {
		this.currentItem = item;
		this._onChange(this.keyAsValue && item ? item.key : item);
	}
	writeValue(item): void {
		if (item !== null) {
			this.currentItem = item;
			this.currentValue = item.key || item;
		}
		if(this.filterOption) {
			this.searchString = this.currentItem && this.currentItem.value;
			this.applyFilter();
		}
	}
	registerOnChange(fn: any): void {
		this._onChange = fn;
	}
	registerOnTouched(fn: any): void {
		this._onTouched = fn;
	}
	setDisabledState?(isDisabled: boolean): void {
		this.disable = isDisabled;
	}
	_onChange = (value: IExtSelectItem | string | number | boolean) => { }
	_onTouched = () => { }
}
