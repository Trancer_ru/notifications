import {ChangeDetectionStrategy, Component, Input, SimpleChanges} from '@angular/core';
import {FormControl, ValidationErrors} from "@angular/forms";
import {BehaviorSubject} from "rxjs";

@Component({
	selector: 'form-errors',
	templateUrl: 'form-errors.html',
	styleUrls: ['form-errors.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormErrorsComponent {
	@Input() errors: ValidationErrors;
	@Input() errorTag: string;
	@Input() dirty: boolean = false;

	errors$ = new BehaviorSubject<string[]>([]);
	errorType: 'error'|'warning'|'success' = 'error';

	constructor() {}

	ngOnChanges(changes: SimpleChanges){
		if(!this.dirty) return;
		const err = (this.errors && Object.keys(this.errors)) || [];
		this.errors$.next(err);
		if(err.length) this.errorType = typeof this.errors[err[0]] === 'string' ? this.errors[err[0]] : 'error';
		//console.log(this.errorType, this.errorTag);
	}

	ngOnInit() {}
}
