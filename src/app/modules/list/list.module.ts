import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { ListComponent } from './list.component';
import {ListRouter} from './list.router';
import {NotificationFormModule} from '@modules/notification-form/notification-form.module';
import {MomentModule} from 'ngx-moment';

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		ListRouter,
		NotificationFormModule,
		MomentModule
	],
	declarations: [ListComponent]
})
export class ListModule {}
