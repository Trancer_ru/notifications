import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {NotificationModel} from '@root/app/models/notification';
import {Observable} from 'rxjs';
import {ModalService} from '@services/modal.service';
import {AlertService} from '@services/alert.service';
import {NotificationFormComponent} from '@modules/notification-form/notification-form.component';
import {NotificationsStore} from '@stores/notifications.store';
import {IonItemSliding, IonRefresher} from '@ionic/angular';
import {StoresService} from '@stores/_stores.service';

@Component({
	selector: 'list-notifications',
	templateUrl: './list.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent implements OnInit {

	notifications$: Observable<NotificationModel[]>;
	private _refresherElement: IonRefresher;

	constructor(
		private _modal: ModalService,
		private _alert: AlertService,
		private _notificationsStore: NotificationsStore,
		private _storesService: StoresService

	) {}

	ngOnInit() {
		this.notifications$ = this._notificationsStore.notifications$;
	}

	add(){
		this._modal.show(NotificationFormComponent, {inputData: {type: 'add'}}).then(
			(modal: any) => {
				modal.onWillDismiss().then( output => {
					if(output.data) this._addNote(output.data);
				});

			});
	}

	private _addNote(note: NotificationModel){
		this._notificationsStore.add(note);
		this._alert.show({
			message: 'Напоминание добавлено'
		});
	}

	remove(note: NotificationModel, slideItem: IonItemSliding){
		slideItem.close();
		this._alert.show({
			message: 'Вы действительно хотите удалить напоминание?',
			buttons: [
				{text: 'Да', handler: _ => this._remove(note.id)},
				{text: 'Нет'},
			]
		});
	}

	private _remove(id: number){
		this._notificationsStore.remove(id);
	}

	edit(note: NotificationModel){
		this._modal.show(NotificationFormComponent,
			{
				inputData: {
					type: 'edit',
					note: note
				}
			}).then(
			(modal: any) => {
				modal.onWillDismiss().then( output => {
					if(output.data) this._editNote(output.data);
				});

			});
	}

	reLoadData(event?){
		if(event) this._refresherElement = event.target;
		this._storesService.loadAll();
		setTimeout(_ => this._refresherElement && this._refresherElement.complete());
	}

	private _editNote(note: NotificationModel){
		this._notificationsStore.edit(note);
		this._alert.show({
			message: 'Напоминание изменено'
		});
	}
}
